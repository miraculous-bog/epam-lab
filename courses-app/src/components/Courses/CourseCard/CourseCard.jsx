import React from 'react';
import PropTypes from 'prop-types';

import Button from '../../../common/Button';

import pipeDuration from '../../../helpers/pipeDuration';

import { Link } from 'react-router-dom';

import { BUTTON_TEXT_SHOW_COURSE } from '../../../constants';

import './courseCard.css';

const CourseCard = ({ data }) => {
	const path = `/courses/${data.id}`;
	return (
		<li className='course-card-container'>
			<div className='left'>
				<h2>{data.title}</h2>
				<p>{data.description}</p>
			</div>
			<div className='right'>
				<p>Authors: {data.authors}</p>
				<p>Duration: {pipeDuration(data.duration)} hours</p>
				<p>Created: {data.creationDate.replace(/[/]/g, '.')}</p>
				<Link to={path}>
					<Button buttonText={BUTTON_TEXT_SHOW_COURSE} />
				</Link>
			</div>
		</li>
	);
};

CourseCard.propTypes = {
	data: PropTypes.shape({
		id: PropTypes.string,
		authors: PropTypes.string,
		title: PropTypes.string,
		creationDate: PropTypes.string,
		description: PropTypes.string,
		duration: PropTypes.number,
	}),
};

export default CourseCard;
