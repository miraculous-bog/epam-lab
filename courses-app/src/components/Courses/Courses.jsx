import React, { useEffect, useState } from 'react';

import CourseCard from './CourseCard';
import SearchBar from './SearchBar';
import Header from '../Header';

import mockedCoursesList from '../../mockedCoursesList';
import { getStringAuthors, getArrEachId } from '../../helpers/formAuthorById';

import './courses.css';

const matchData = () => {
	return mockedCoursesList.map((course) => {
		return {
			...course,
			authors: getStringAuthors(getArrEachId(course.authors)),
		};
	});
};

const Courses = () => {
	const [inputField, setInputField] = useState('');
	const [dataPosts, setDataPosts] = useState([]);
	useEffect(() => {
		if (inputField === '') {
			setDataPosts(matchData());
		}
	}, [inputField]);
	const handleInputChange = (e) => {
		const { value } = e.target;
		setInputField(() => value);
	};

	const handleButtonSerach = () => {
		let temporaryData = matchData();
		const filtredData = temporaryData.filter(
			(post) =>
				post.id.toLowerCase().includes(inputField.toLowerCase()) ||
				post.title.toLowerCase().includes(inputField.toLowerCase())
		);
		setDataPosts(filtredData);
	};

	return (
		<>
			<Header />
			<div className='wrapper'>
				<SearchBar
					value={inputField}
					handleInputChange={handleInputChange}
					handleButtonSerach={handleButtonSerach}
				/>
				<ul>
					{dataPosts.map((item) => {
						return <CourseCard key={item.id} data={item} />;
					})}
				</ul>
			</div>
		</>
	);
};

export default Courses;
