import React from 'react';

import Logo from './Logo';
import Button from '../../common/Button';

import { useAuth } from '../../hook/useAuth';
import { useNavigate } from 'react-router-dom';

import './header.css';

const Header = () => {
	const { user, signout } = useAuth();
	const navigate = useNavigate();
	return (
		<div className='header'>
			<Logo />
			{user && (
				<div className='profile'>
					<p className='text'>{user}</p>
					<Button
						buttonText='Logout'
						actionBtn={() =>
							signout(() => navigate('../login', { replace: true }))
						}
					/>
				</div>
			)}
		</div>
	);
};

export default Header;
