import mockedAuthorsList from '../mockedAuthorsList';
import { CUT_AUTHORS_NAME_LENGTH } from '../constants';

const getAuthorWithId = (id) =>
	mockedAuthorsList.find((author) => author.id === id).name;

const getStringAuthors = (arrAuthors, doAx = true) => {
	const readyStr = arrAuthors.join(', ');
	return doAx && readyStr.length > CUT_AUTHORS_NAME_LENGTH
		? readyStr.slice(0, CUT_AUTHORS_NAME_LENGTH) + '...'
		: readyStr;
};

const getArrEachId = (courseAuthorsId) => {
	return courseAuthorsId.map((authorId) => getAuthorWithId(authorId));
};

export { getAuthorWithId, getStringAuthors, getArrEachId };
