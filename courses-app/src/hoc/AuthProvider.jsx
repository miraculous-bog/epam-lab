import React from 'react';
import PropTypes from 'prop-types';
import { createContext, useState } from 'react';

export const AuthContext = createContext(null);

export const AuthProvider = ({ children }) => {
	const [user, setUser] = useState(null);

	const signin = (newUser, cb) => {
		setUser(newUser);
		cb();
	};
	const signout = (cb) => {
		localStorage.clear();
		cb();
		setUser(null);
	};

	const value = { user, signin, signout };

	return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

AuthProvider.propTypes = {
	children: PropTypes.element,
};
